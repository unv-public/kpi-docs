---
title: Contract Extensions Rejection Rate (%age)
---

# Contract Extensions Rejection Rate

This KPI measures the percentage of Volunteer Contract Extension Requests that were rejected by UNV. 

## Purpose of this KPI

Host Entities make contract extension requests, these contract extension requests are expected to meet some minimum criteria e.g. they need to have a funding confirmation. 

In cases where these minimum criteria are not met, Operations Associates in Volunteer Management can reject a contract extension request.

This KPI is a qualitative measure that can be used to:
 - measure the overall completion quality of submitted contract extension requests
 - identify specific problem areas in contract extension requests - for e.g. if via the analysis page of the KPI it is evident that certain host-entities or certain kinds of positions (e.g. Mission positions) have a higher rate of contract extension rejection - it can allow for very specific corrective action. 

## How it Works

This KPI looks at all Contract Extension requests (value Z), and then looks at all Contract Extension requests that were Rejected (value Y).  It then sees what percentage of all Contract  extension requests were Rejected. 

``` math
(Y ÷ Z) X 100 = 10%
```

So for example, if there were 100 contract extension requests and 11 of these were Rejections. The value of the KPI would be:

``` math
(11 ÷ 100) X 100 = 11%
```



