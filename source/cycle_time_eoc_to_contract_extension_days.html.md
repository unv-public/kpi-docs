---
title: Cycle Time End of Contract (EOC) to Contract Extension in Days
---

# Cycle Time End of Contract (EOC) to Contract Extension in Days

This KPI measures the average number of days between when Contract Extension Request was made by the Host Entity and the Expected End Date of the Contract

## Purpose of this KPI

Host Entities are expected to place Contract Extension Requests for Contracts they intend to extend at least 60 days before the Expected End date of the Contract. 

For example, if Contract A has an Expected End Date of March 31st 2022, as per UNV requirements the Host Entity should have placed a Contract Extension request by January 30th, 2022 (60 days prior). 

This KPI can be used for qualitative purposes to measure which Host Entities are prompt in making Contract Extension Requests and which ones are not. 

It can also provide an overall measure of how all the host entities are performing in terms of making Contract extension requests on time. 

For instance - if the KPI is indicating 30 days - it means most Host Entities are waiting till late in the day to make Contract Extension requests - and this means that some corrective action is required on the part of Host Entities to submit Extension Requests in a timely manner. 

## How it Works

This KPI looks at only approved Contract Extension requests, and takes an average of the number of days between when the Extension Request was made to when the Contract was expected to end. 

We consider only Approved Contract Extension requests to ensure that Extension Requests made are qualitatively valid - i.e. we don't want to include Contract Extension Requests which were made on time but were rejected for qualitative reasons.