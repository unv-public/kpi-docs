---
title: Host-Entity Recall Rate
---

# Host-Entity Recall Rate

This KPI measures the percentage of Selected Candidates ranked in the top 20% of the list by AI across all Candidate matches for a DoA.

## Purpose of this KPI

The intention of this KPI is to track the quality of AI ranking system. This is a sister KPI to [Recruiter Recall Rate](./recruiter_recall_rate.html). While that tracks AI quality with respect to Shortlists, this KPI tracks AI quality with respect to selections.

It monitors the quality of the AI scoring with respect to Selection outcomes.

We explain the scenario with 2 different cases:

- Case A: AI suggests high score candidates in Longlists and most candidates who got Selected fell within the high score range (This is a case of _Good AI performance_ - the AI is largely aligned to what Candidate Identification personnel are doing and what the Host Entity selected )
- Case B: - AI suggests high score candidates in Longlists but few candidates who got Selected had a high score (This is a case of _Poor AI performance_ - something is wrong with the AI scoring system and needs readjustment)

Case B would suggest there is something wrong with the scoring system of the AI and it needs improvement.

## How it Works

This KPI examines Selected Candidates.

We get the Selected candidates who were ranked in the top 20% of the long-list, and get a percentage of those in the overall Selection.

For example -

If 10 candidates were Selected, and they had longlisting scores as per AI as follows:

![https://gitlab.com/unv-public/kpi-documentation/-/wikis/uploads/546554eb780839ab6f90d09bec621ced/image.png](https://gitlab.com/unv-public/kpi-documentation/-/wikis/uploads/546554eb780839ab6f90d09bec621ced/image.png)

If only Candidates E and F were in the 20% score of the original long-list. The KPI would be calculated as :

``` math
(2 ÷ 10) X 100 = 20%
```

This indicates that the AI is not really working well since only 20% of the candidates recommended by the AI as falling in the top percentile were Selected, in fact 20% of the candidates ( Candidates D and C) with a less than 6% ranking score were selected.