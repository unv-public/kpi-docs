---
title: Recruiter Recall Rate
---

# Recruiter Recall Rate

This KPI measures the percentage of Shortlisted Candidates ranked in the top 20% of the list by AI across all Candidate matches for a DoA.

## Purpose of this KPI

The intention of this KPI is to track the quality of AI ranking system. We know that the AI is likely not going to provide good quality outputs from Day 1 - that is because of the nature of the technology - it gets better over time by learning from user made choices.

We have existing AI KPIs that measure what percentage of candidates who got a high score in longlists, but it does not tell us whether that high score was actually correct .

This KPIs provide that qualitative info on the AI - it monitors the quality of the AI scoring with respect to different kinds of short-listing outcomes.

There are basically 2 kinds of possibilities here for Short-listing-

- Case A: - AI suggests high score candidates in Longlists and most candidates who got Short-listed had a high score (This is a case of _Good AI performance_ - the AI is largely aligned to what Candidate Identification personnel are doing and what the Host Entity expects )
- Case B: - AI suggests high score candidates in Longlists but few candidates who got Short-listed had a high score (This is a case of _Poor AI performance_ - something is wrong with the AI scoring system and needs readjustment)

The analogy would be that of students going to 2 different schools A and B - and getting high grades (95%) in Math in both schools - but students from School B consistently FAIL Math tests done independently with them, while students from School A consistently PASS the same independent Math tests.

This suggests there is something wrong with the teaching / scoring system in School B.

## How it Works

This KPI examines Shortlisted Candidates.

We get the Shortlisted candidates who were ranked in the top 20% of the long-list, and get a percentage of those in the overall short-list.

For example -

If 10 candidates were shortlisted, and they had longlisting scores as per AI as follows:

![https://gitlab.com/unv-public/kpi-documentation/-/wikis/uploads/546554eb780839ab6f90d09bec621ced/image.png](https://gitlab.com/unv-public/kpi-documentation/-/wikis/uploads/546554eb780839ab6f90d09bec621ced/image.png)

If only Candidate E was in the 20% of the original long-list with a score of 74.07. The KPI would be calculated as :

``` math
(1 ÷ 10) X 100 = 10%
```

This indicates that the AI is not really working well since only 10% of the candidates recommended by the AI as falling in the top percentile were shortlisted.